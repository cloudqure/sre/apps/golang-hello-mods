FROM golang:1.12 as builder
WORKDIR /usr/src/app

# Set Environment Variables
ENV CGO_ENABLED 0
ENV GOOS linux

COPY . .
RUN go mod download
RUN go build -v -a -installsuffix cgo -o app .

FROM alpine:3.5
# We'll likely need to add SSL root certificates
RUN apk --no-cache add curl ca-certificates

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/app/app .
CMD ["./app"]
