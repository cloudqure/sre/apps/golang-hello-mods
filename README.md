# Description 


## Usage: 

```bash 
# manually build and run local image: 
docker build -t "local/${PWD##*/}" .
docker run --rm -p 8080:800 "local/${PWD##*/}"

# ...alternatively, run: 
docker-compose up
```