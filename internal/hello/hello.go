package hello

// Hello returns a simple test string
func Hello() string {
	return "Hello, world!"
}
